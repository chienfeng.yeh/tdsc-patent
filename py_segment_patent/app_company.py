import joblib
import progressbar
from pymongo import MongoClient

def loadData():
    j=joblib.load('app_need_ipc.pkl')
    pgb.start(len(j))
    pgb_count=0
    for num in j:
        cursor = coll.find({'patent-detail.specification.@application-num': num})
        try:
            for doc in cursor:
                company = doc['company']
                result[num] = company
        except:
            result[num] = ''
        pgb_count+=1
        pgb.update(pgb_count)

def dumpResult():
    joblib.dump(result,'app_company.pkl')

uri = "mongodb://h760127:keroro76@git.h760127.com:27017/?authSource=admin&authMechanism=SCRAM-SHA-1"
client = MongoClient(uri)
db = client.patent
coll = db.patent_tw_detail
result={}
pgb=progressbar.ProgressBar()
loadData()
dumpResult()