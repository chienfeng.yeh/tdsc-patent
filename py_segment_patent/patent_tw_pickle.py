'''
Created on 2018年7月23日
  
@author: pig98520
'''
import html,pickle
from pymongo import MongoClient
from bs4 import BeautifulSoup

def checkFiled(filed_list):
    outer=doc
    for filed in filed_list:
        if(filed in outer):
            outer=outer[filed]
        else:
            return False
    return True
  
def getContent(doc):
    text=''
    if(checkFiled(['patent-detail','specification','description'])):
        des=html.unescape(doc['patent-detail']['specification']['description'])
        if(isinstance(des, list)):
            des=des[1]
        bs=BeautifulSoup(des,'html.parser')
        ds=bs.find_all('description')[0]
        name_list=['technical-field','background-art','disclosure','mode-for-invention']
        for p in ds.find_all(name_list):
            text+=p.text.replace('\n','')
    return text
    
def dumpPkl(data_list):
    f=open('./output/patent_tw.pkl','ab+')
    pickle.dump(data_list,f)
    f.close()
   
uri = "mongodb://h760127:keroro76@git.h760127.com:27017/?authSource=admin&authMechanism=SCRAM-SHA-1"
client = MongoClient(uri)
db = client.patent
coll = db.patent_tw_detail
cursor=coll.find({},no_cursor_timeout = True).batch_size(8000)
# cursor=coll.find({},no_cursor_timeout = True).sort('_id',pymongo.DESCENDING).limit(73381).batch_size(8000)
# cursor=coll.find({},no_cursor_timeout = True).sort({'_id',pymongo.ASCENDING}).limit(100000).batch_size(8000)
data_list=[]
count=0
for doc in cursor:
    title=doc['patent_name'] if 'patent_name' in doc else ''
    app_num=doc['patent-detail']['specification']['@application-num'] if checkFiled(['patent-detail','specification','@application-num']) else ''
    try:
        ipc=doc['patent-detail']['specification']['@ipc-mark'] if checkFiled(['patent-detail','specification','@ipc-mark']) else ''
        status=doc['patent-detail']['specification']['@status'] if checkFiled(['patent-detail','specification','@status']) else ''
        content=getContent(doc)
        txt_list=[title,app_num,ipc,status,content]
        data_list.append(txt_list)
        count+=1
        if(count%10==0):
            print('dumping...')
            dumpPkl(data_list)
            data_list=[]
#         print('title:{},app_num:{},ipc:{},status:{} \ncontent:\n{}'.format(title,app_num,ipc,status,content))
        print(count)
    except Exception as e:
        f=open('./output/error.txt','a',encoding='utf8')
        f.write(app_num+' '+str(e)+'\n')
        f.close()