import pickle
import jieba
import progressbar
import glob
import ipc_filter
import unicodedata
import re

def segmentation(punctuation, stop_word, patent, time_count):
    #content segmentation
    print("start segmenting...")
    file_count = 1
    patternContent = re.compile(r'[^\u4e00-\u9fa5\+a-zA-Z-]')
    patternFull = re.compile(r'[A-Z]\d{1,2}[A-Z]* \d+/\d+|[A-Z]\d{1,2}[A-Z]*')
    seg_count = 0
    ipc_dict = {}
    page_count = 0
    bar = progressbar.ProgressBar(max_value=progressbar.UnknownLength)
    for content in patent[:]:
        if seg_count < 10000:
            if len(content[4]) > 0 and len(content[2]) > 0: #length of content > 0, IPC != ""
                content[4] = (unicodedata.normalize('NFKC', content[4])).lower()
                content[4]=re.sub(patternContent,'',content[4])
                seg_list = list(jieba.cut(content[4], cut_all=False))
                ipc_list=ipc_filter.filterIPC(content[2]) #filter_ipc
                v1 = list(filter(lambda x: x not in punctuation, seg_list))   #text without punctuation
                v2 = list(filter(lambda x: x not in stop_word, v1))     #text without stopword
                v3 = [v.strip() for v in v2 if v != ' ' and v!= ""]
                #print(v3)
#                 new_dict[content[1]] = v3#application num, segmented content
                for ipc in ipc_list:
                    if(ipc not in ipc_dict):
                        ipc_dict[ipc]={}
                        ipc_dict[ipc][content[1]] = v3
                    else:
                        ipc_dict[ipc][content[1]] = v3
                patent.remove(content)
            else:
                patent.remove(content)
            seg_count += 1
        elif seg_count == 10000:
            for ipc in ipc_dict:
                if(patternFull.fullmatch(ipc)):
                    with open('./output_ipc_filter/segmented_content_{}.pkl'.format(ipc.replace('/','-')), 'ab') as c:
                        pickle.dump(ipc_dict[ipc], c)
                    print('segmented_content_{}.pkl has been save.'.format(ipc))
                else:
                    with open('./output_ipc_filter/segmented_content_{}.pkl'.format('Others'), 'ab') as c:
                        pickle.dump(ipc_dict[ipc], c)
                    print('segmented_content_{}.pkl has been save.'.format(ipc))
            # with open("./output_ipc_filter/segmented_content_{}_{}.pkl".format(time_count, file_count), "wb")as c:
            #     pickle.dump(ipc_dict, c)
            # print("segmented_content_{}_{}.pkl has been save.".format(time_count, file_count))
            seg_count = 0
            ipc_dict = {}
            file_count += 1
        page_count += 1  # bar
        bar.update(page_count)  # bar

    if len(ipc_dict.keys()) > 0:
        for ipc in ipc_dict:
            if (patternFull.fullmatch(ipc)):
                with open('./output_ipc_filter/segmented_content_{}.pkl'.format(ipc.replace('/','-')), 'ab') as c:
                    pickle.dump(ipc_dict[ipc], c)
                print('segmented_content_{}.pkl has been save.'.format(ipc))
            else:
                with open('./output_ipc_filter/segmented_content_{}.pkl'.format('Others'), 'ab') as c:
                    pickle.dump(ipc_dict[ipc], c)
                print('segmented_content_{}.pkl has been save.'.format(ipc))

        # with open("./output_ipc_filter/segmented_content_{}_{}.pkl".format(time_count,file_count), "wb")as c:
        #     pickle.dump(ipc_dict, c)

# punctuations
with open("./punctuation.txt", 'r', encoding='UTF-8') as c:
    pun = list(c.read())
punctuation = [p for p in pun if pun.index(p) % 2 == 0]
print("punctuation.txt has been loaded.")

# stopwords
with open("./stopword_chinese.txt", 'r', encoding='UTF-8') as c:
    stop = c.readlines()
stop_word = [s.strip() for s in stop]
print("stopword_chinese.txt has been loaded.")

# load term chinese dict
path = "./all_ch_tw_part*.txt"
file_list = [f for f in glob.glob(path)]
for file in file_list:
    jieba.load_userdict(file)
    print(file, " loaded")

# load term eng dict
path = "./all_en.txt"
file_list = [f for f in glob.glob(path)]
for file in file_list:
    jieba.load_userdict(file)
    print(file, " loaded")


#patent
"""
with open("paper_xml.pkl", "rb")as c:
    patent = pickle.load(c)
"""

print("start loading patent content...")

time_count = 1
patent = []
with open("patent_tw.pkl", "rb")as c:
    while True:
        try:
            patent_part = pickle.load(c)
            patent.extend(patent_part)
        except:
            break
        # patent=sorted(patent, key=lambda x:x[2])
        if len(patent) == 10000:
            segmentation(punctuation, stop_word, patent, time_count)
            time_count += 1
            patent = []

if len(patent) > 0:
    segmentation(punctuation, stop_word, patent, time_count)
