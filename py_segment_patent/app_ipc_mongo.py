import pickle
import progressbar
from pymongo import MongoClient
import ipc_filter

def loadIPC():
    result = {}
    with open('./output/cal_py/app_need_ipc.pkl', 'rb') as f:
        p = pickle.load(f)
    pgb.start(len(p))
    pgb_count = 0
    for num in p:
        cursor = coll.find({'patent-detail.specification.@application-num': num},
                           {'patent-detail.specification.@ipc-mark': 1});
        for doc in cursor:
            ipc = doc['patent-detail']['specification']['@ipc-mark']
            result[num] = ipc_filter.filterIPC(ipc)
        p.remove(num)
        pgb_count += 1
        pgb.update(pgb_count)
        if (len(result)==1000):
            dumpPkl(result)
            result={}
    if(len(result)>0):
        dumpPkl(result)

def dumpPkl(result):
    with open('./output/app_ipc.pkl', 'ab') as f:
        pickle.dump(result,f)

pgb=progressbar.ProgressBar()
uri = "mongodb://h760127:keroro76@git.h760127.com:27017/?authSource=admin&authMechanism=SCRAM-SHA-1"
client = MongoClient(uri)
db = client.patent
coll = db.patent_tw_detail
loadIPC()
