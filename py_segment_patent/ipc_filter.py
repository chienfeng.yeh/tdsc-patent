'''
Created on 2018年8月1日

@author: pig98520
'''
import re
from pymongo import *

def filterIPC(ipc):
    global patternClass, patternGroup, patternMatch, patternFull
    #只含部跟類 只含目 只含部跟類/目 完整包含部跟類(目)
    #A01A 110/01
    ipc=re.sub(r'[^\w/]',' ',ipc)
    ipc=ipc.upper()
    patternClass = re.compile(r'[A-Z]\d{1,2}[A-Z]*')
    patternGroup = re.compile(r'\d+/\d+')
    patternMatch = re.compile(r'[A-Z]\d{1,2}[A-Z]*|\d+/\d+')
    patternFull = re.compile(r'[A-Z]\d{1,2}[A-Z]* \d+/\d+|[A-Z]\d{1,2}[A-Z]*')
    try:
        if(len(patternClass.findall(ipc))!=len(patternGroup.findall(ipc)) and len(patternClass.findall(ipc))<len(patternGroup.findall(ipc))):
            #部類與目數量不同
            ipc_list=formatDirtyIPC(patternMatch.findall(ipc))
        elif(len(patternClass.findall(ipc))==len(patternGroup.findall(ipc)) and len(patternClass.findall(ipc))!=0):
            #部類與目數量相同
            ipc_list=formatCleanIPC(patternMatch.findall(ipc))
        elif(len(patternGroup.findall(ipc))==0 and len(patternClass.findall(ipc))!=0):
            #只包含部跟類
            ipc_list=patternClass.findall(ipc)
        elif(len(patternGroup.findall(ipc))<len(patternClass.findall(ipc)) and len(patternGroup.findall(ipc))!=0):
            ipc_list = patternFull.findall(ipc)
        else:
            ipc_list=[ipc]
    except:
        ipc_list=[ipc]
    return ipc_list

def formatDirtyIPC(ipc_list):
    formatted = []
    ipc = ''
    for ele in ipc_list:
        if (patternClass.match(ele)):
            ipc = ele
        else:
            formatted.append(ipc + ' ' + ele)
    return formatted


def formatCleanIPC(ipc_list):
    formatted = []
    for i in range(0, len(ipc_list), 2):
        formatted.append(ipc_list[i] + ' ' + ipc_list[i + 1])
    return formatted

def captureIPC(ipc): #只擷取部跟類
    return patternClass.findall(ipc)

def mongoIPC():
    uri = "mongodb://h760127:keroro76@git.h760127.com:27017/?authSource=admin&authMechanism=SCRAM-SHA-1&ssl=true"
    client = MongoClient(uri)
    db = client.patent
    coll = db.patent_tw_detail
    cursor = coll.distinct('patent-detail.specification.@ipc-mark')
    ipc_list=[]
    #IPC含五個等級
    ipc_set=set()
    #IPC只含部跟類
    for ipc in cursor:
        ipc_list+=filterIPC(ipc)
        print(ipc, filterIPC(ipc))
    ipc_list=list(set(ipc_list))
    #取得不重複的ipc
    ipc_list.sort()
    for ipc in ipc_list:
        if(patternFull.fullmatch(ipc)):
            if(not set(captureIPC(ipc)).issubset(ipc_set)):
                ipc_set.update(captureIPC(ipc))
        #     with open('cleanIPC.txt','a') as f:
        #         f.write(ipc+'\n')
        # else:
        #     with open('dirtyIPC.txt','a') as f:
        #         f.write(ipc+'\n')
        # print(ipc+' saved.')
    for i in sorted(ipc_set):
        print(i)
# mongoIPC()