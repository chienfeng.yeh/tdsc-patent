import pickle
import progressbar
import ipc_filter

def dumpPkl(dic):
    with open('app_ipc.pkl','ab') as f:
        pickle.dump(dic,f)

contents=[]
pgb=progressbar.ProgressBar(max_value=progressbar.UnknownLength)
pgb_count=0
f=open('./patent_tw.pkl','rb')
while True:
    try:
        all_dict = {}
        for i in pickle.load(f):
            if(i[2]!=''):
                all_dict[i[1]] = ipc_filter.filterIPC(i[2])
                dumpPkl(all_dict)
        pgb_count+=1
        pgb.update(pgb_count)
    except EOFError:
        break