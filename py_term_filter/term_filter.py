'''
Created on 2018年7月7日

@author: pig98520
'''
import re,pickle
def strF2W(input_str):
    input_str=input_str.replace(' ','')
    output_str=''
    for char in input_str:
        inside_code = ord(char)
        #將文字轉成編碼
        if inside_code == 12288:
            inside_code = 32
            #全形空格直接轉換
        elif (inside_code >= 65281 and inside_code <= 65374):
            inside_code -= 65248
            output_str+= chr(inside_code)
            #全形字元（除空格）根據關係轉化
        else:
            output_str += chr(inside_code)
    output_str.strip()
    return output_str

def pickleDump(datas,file_name):
    datas.insert(0, ["ch_tw", "en", "IPC", "ch_cn"])
    f=open('./output/'+file_name,'wb')
    pickle.dump(datas,f)
    del datas[0]
    f.close()

def writeTxt(datas,file_name):
    f=open('./output/'+file_name,'a',encoding='utf8')
    for data in datas:
        f.write(data[0]+'\n')
    f.close

def writeCompare(datas,file_name):
    f=open('./output/'+file_name,'a',encoding='utf8')
    for data in datas:
        print('Before:{} ,After:{}'.format(data[0],data[1:]))
        f.write('Before:{} ,After:{}\n'.format(data[0],data[1:]))
    f.close

# pkl_file='ch_tw_1'
pkl_file=input('please enter file name:')
pattern = re.compile(r'[^\u4e00-\u9fa5\w]')
#非繁體中文及英文數字者
f=open('{}.pkl'.format(pkl_file),'rb')
datas=pickle.load(f)
filters=[]
unfilters=[]
#存放pkl用
filter_list=[]
unfilter_list=[]
#存放原始字詞比較差異用
for data in datas[1:]:
    data[0]=strF2W(data[0])
    #先將字符中的非中文的全形全部轉為半形
    match=pattern.search(data[0])
    if(match):
        temps=[]
        unfilter_list.append([data[0]])
        #用原始字詞append一個新的list
        temps=(re.findall(r'[\u4e00-\u9fa5]+|[A-Za-z]+',data[0]))
        #將原始字詞找出可用的規則:純中文單字|純英文單字
        for t in re.findall(r'[A-Z]-*[\u4e00-\u9fa5]+',re.sub('-[a-zA-Z]-','',data[0])):
            if(t not in temps):
                temps.append(t)
        #例外處裡[英文-中文]的狀況,例如[X-射線,V型皮夾]
        if(re.compile(r'[^\u4e00-\u9fa5\+\w-]').search(data[0])):
            orignal=re.sub(r'[^\u4e00-\u9fa5a-zA-Z]','', data[0])
        else:
            orignal=data[0]
        if(orignal not in temps):
            temps.insert(0,(orignal))
        #處裡原始字詞,若其中包含非數字、中文或+-則將其去除(只留下中文及英文部分,其餘可能伴隨特殊符號形成冗字),接著插入list最前面
        for t in temps:
            if(t!=''):
                unfilter_list[len(unfilter_list)-1].append(t)
                if(t==orignal):
                    unfilters.append([t,data[1],data[2],data[3]])
                else:
                    unfilters.append([t,None,None,None])
                    #unfilters.append([t,data[1],data[2],data[3]])
                    #若為分割後的詞,則不帶入其他屬性;亦即屬性只帶入標題處裡過後的詞
    else:
        filters.append(data)
        filter_list.append([data[0]])
f.close()

# pickleDump(filters, '{}_filter.pkl'.format(pkl_file))
# pickleDump(unfilters, '{}_unfilter.pkl'.format(pkl_file))
# writeTxt(filter_list, '{}_filter.txt'.format(pkl_file))
# writeTxt(unfilter_list, '{}_unfilter.txt'.format(pkl_file))
# writeCompare(unfilter_list,'{}_compare.txt'.format(pkl_file))