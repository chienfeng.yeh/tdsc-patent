import xml.etree.cElementTree as ET
import pickle
  
  
def transfer(input_filename, output_filename, create_txt):
    tree = ET.ElementTree(file = input_filename)
    root = tree.getroot()
    #term pickle file
    # term in chinese tw, term in english, IPC(None or like -H/01/L/31/352 -H/04/J/14/2), term in simple chinese
    if "ChtSimple" not in input_filename:
        transfer_list = [[child[1].text, child[2].text, child[4].text, None] for child in root]
    else:
        transfer_list = [[child[1].text, None, None, child[2].text] for child in root]
     
    transfer_list.insert(0, ["ch_tw", "en", "IPC", "ch_cn"])
    print("10 examples of the transfered data : ")
    for i in transfer_list[:10]:
        print(i)
    
    with open("{}.pkl".format(output_filename), "wb")as c:
        pickle.dump(transfer_list, c)
     
    #txt for jieba
    if create_txt.lower() == "y":
        f = open('jieba_{}.txt'.format(output_filename), 'w', encoding='UTF-8')
        for i in transfer_list[1:]:
            f.write(i[1]+"\n")
        f.close()
        print("==============================")
        print("jieba dict has been created.")
  
filename_list = [["./ChtEngMapping.xml", 'ch_tw_1'], ["./ChtEngMapping2.xml", 'ch_tw_2'], ["./ChtSimpleMapping.xml", 'ch_cn']]
for i in range(0, len(filename_list)):
    print(i, filename_list[i][0])
print("==============================")
filename_index = int(input("Please choose the number of input filename : "))
create_txt = input("Are you going to create term dict for jieba ? (y/n) ")
print("==============================")
transfer(filename_list[filename_index][0], filename_list[filename_index][1], create_txt)